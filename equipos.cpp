//@author: Eduardo Juarez - Departamente de sistemas computacionales - Tecnol�gico de Monterrey, Campus Quer�taro
//@version: 1.1
//@date: 9/02/2014

/**
 * Programa para la generaci�n de equipos aleatorios.
 *
 * El programa recibe como entrada un archivo llamado alumnos.txt que contiene las matr�culas
 * de los alumnos, y genera el archivo equipos.txt con los equipos
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <iostream>
#include <fstream>
#define MAX 50

using namespace std;

/**
 * Inicializa un arreglo de enteros a 0
 * @param arreglo: El arreglo a inicializar
 */
void inicializaArreglo(int arreglo[])
{
    for (int i = 0; i < MAX; i++)
    {
        arreglo[i] = 0;
    }
}

/**
 * Asigna los alumnos a sus equipos de trabajo.
 * La funci�n tambi�n valida que los equipos de trabajo tengan la cantidad correcta de integrantes
 * @param cantidadEquipos: La cantidad de equipos
 * @param contadorIntegrantes: Arreglo que va contando el n�mero de integrantes en cada equipo
 * @param alumnosXEquipo: Los alumnos que debe tener cada equipo
 * @param residuoAlumnos: Los alumnos que hay dem�s por los que no se puede tener la cantidad exacta
 *                        de integrantes en cada equipo
 * @param equipo: Arreglo donde se guarda la asignaci�n del equipo para cada alumno
 * @param i: Indica el alumno actual
 */
int asignarEquipo(int cantidadEquipos, int contadorIntegrantes[], int alumnosXEquipo,
                              int &residuoAlumnos, int equipo[], int i )
{
    int equipoActual = rand() % cantidadEquipos;
    cout << "Random:" << equipoActual << endl;
    contadorIntegrantes[equipoActual]++;
    if (contadorIntegrantes[equipoActual] > alumnosXEquipo)
    {
        if (residuoAlumnos > 0)
        {
            if (contadorIntegrantes[equipoActual] > alumnosXEquipo+1)
            {
                //cout << "Recalculando por residuo > que 0..." << endl;
                asignarEquipo(cantidadEquipos, contadorIntegrantes, alumnosXEquipo, residuoAlumnos, equipo, i);
                return 0;
            }
            else
            {
                //cout << "Equipo con mas integrantes" << endl;
                residuoAlumnos--;
            }
        }
        else
        {
            //cout << "Recalculando..." << endl;
            asignarEquipo(cantidadEquipos, contadorIntegrantes, alumnosXEquipo, residuoAlumnos, equipo, i);
            return 0;
        }
    }
    equipo[i] = equipoActual;
    return 0;
}

int main()
{
    srand(time(NULL));
    int cantidadEquipos;
    ifstream entrada;
    ofstream salida;
    int equipoNumero;
    string alumnos[50];
    int equipo[MAX];
    int contadorIntegrantes[MAX];
    int i=0;
    int cantidadAlumnos;
    int alumnosXEquipo;
    int residuoAlumnos;

    inicializaArreglo(contadorIntegrantes);
    inicializaArreglo(equipo);

    entrada.open("alumnos.txt");
    salida.open("equipos.txt");

    cout << "Escribe la cantidad de equipos: ";
    cin >> cantidadEquipos;

    //Lectura del archivo de entrada que contiene las matr�culas de cada alumno
    while (!entrada.eof())
    {
        entrada >> alumnos[i];
        cout << alumnos[i] << "\n";
        i++;
    }

    //C�lculo de la cantidad de alumnos que debe de haber en cada equipo y cu�ntos sobran
    cantidadAlumnos = i-1;
    alumnosXEquipo = cantidadAlumnos / cantidadEquipos;
    residuoAlumnos = cantidadAlumnos % cantidadEquipos;

    cout << "Alumnos " << cantidadAlumnos << endl;
    cout << "Alumnos por equipo: " << alumnosXEquipo << endl;

    //Asignaci�n de los alumnos a sus equipos
    for (i = 0; i < cantidadAlumnos; i++)
    {
        asignarEquipo(cantidadEquipos, contadorIntegrantes, alumnosXEquipo, residuoAlumnos, equipo, i);
        cout << "Alumno " << i << ": " << equipo[i] << endl;
    }

    //Impresi�n de resultados
    for (equipoNumero = 1; equipoNumero <= cantidadEquipos; equipoNumero++)
    {
        salida << "Equipo " << equipoNumero << ":" << endl;
        cout << "Equipo " << equipoNumero << ":" << endl;
        for (int j = 0; j < cantidadAlumnos; j++)
        {
            if (equipo[j] == equipoNumero-1)
            {
                salida << alumnos[j] << endl;
                cout << alumnos[j] << endl;
            }
        }
        salida << endl;
        cout << endl;
    }

    entrada.close();
    salida.close();
    cout << "Equipos generados en el archivo equipos.txt" << endl;
    //system("PAUSE");
}
